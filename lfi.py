import threading
import time
import argparse
import sys
import mechanize
import csv

urls = []
data_table = []
failed_table = []
start = time.time()


def get_args():
        parser = argparse.ArgumentParser(description='LFI Scanner with multi threading and socks5 support')
        parser.add_argument('-u', '--url', type=str, help='Enter a url i.e "http://wwww.google.com"', required=False)
        parser.add_argument('-f', '--file1', type=str, help='Filename i.e "urls.txt"', required=False)
        if len(sys.argv) == 1:
            parser.print_help()
            sys.exit(1)
        elif len(sys.argv) > 3:
            print("Please choose only one option")
            sys.exit(1)
        args = parser.parse_args()
        url = args.url
        file1 = args.file1
        if url is not None:
            list1 = [url]
            return list1
        elif file1 is not None:
            file_data = open(file1, "r")
            file_data = list(file_data)
            return file_data


def open_url(url):
    br = mechanize.Browser()
    br.set_handle_equiv(True)
    br.set_handle_redirect(True)
    br.set_handle_referer(True)
    br.set_handle_robots(False)
    br.addheaders = [('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0')]
    # br.set_proxies({"socks5": address+':'+port})
    return br.open(url)


def fetch_data_set():
    f = open_url("https://raw.githubusercontent.com/danielmiessler/SecLists/master/Fuzzing/JHADDIX_LFI.txt")
    return f


def request_prepare(url):
    data_set = fetch_data_set()
    for data_row in data_set:
        a = url+data_row
        urls.append(a)
    return urls


def request_validator(url):
    try:
        url = open_url(url)
        if url.getcode() == 403 or url.getcode() == 302 or url.getcode() == 404:
            return False
        else:
            return url
    except:
        return False


def check_response(url):
    url = url.strip("\\n")
    checker = request_validator(url)
    if checker:
        data_table.append(url)
        print(data_table)
    else:
        failed_table.append(url)

targets = get_args()
print(targets)
for target in targets:
    request_prepare(target)


def urlthreading():
    threads = [threading.Thread(target=check_response, args=(url,)) for url in urls]
    for thread in threads:
        thread.start()
    for thread in threads:
        thread.join()


urlthreading()
# ##Writing Failed Results
resultFile = open("successful.csv", 'wb')
wr = csv.writer(resultFile, dialect='excel')
wr.writerow(data_table)
# ##Writing Successful Results
resultFile = open("failed.csv", 'wb')
wr = csv.writer(resultFile, dialect='excel')
wr.writerow(failed_table)


print("Elapsed Time: %s" % (time.time() - start))
